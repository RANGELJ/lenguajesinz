/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lector;

/**
 *
 * @author mayro
 */
public class TokenV extends Token {

    private final String tipo;
    private final String identificador;

    public TokenV(String valor, int numero, String tipo, String identificador) {
        super(valor, numero, 0, 0, 0);
        this.tipo = tipo;
        this.identificador = identificador;
    }

    public String getTipo() {
        return tipo;
    }

    public String getIdentificador() {
        return identificador;
    }

}
