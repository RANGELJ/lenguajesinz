/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lector;

import CI.CIAsignacionSimple;
import CI.CIDeclaracion;
import CI.CIGoto;
import CI.CIIf;
import CI.CIPrint;
import CI.CIRead;
import CI.Codigo;
import CI.Condiciones.CondIf;
import CI.Etiquetas;
import CI.RowEtiquetas;
import cuadruples.Cuadruple;
import cuadruples.Cuadruples;
import java.util.LinkedList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import lector.tablaSimbolos.RowTablaSimbolos;
import lector.tablaSimbolos.TablaSimbolos;
import postfijo.Postfijo;
import postfijo.ResPost;

/**
 *
 * @author
 */
public class Semantico {

    // TABLA DE SIMBOLOS
    public static TablaSimbolos tablaDeSimbolos = new TablaSimbolos();

    // Guarda temporalmente el tipod e dato para colocarlo en la tablad e simbolos
    private static String tipoDeDatoActual = "";
    
    // Indicador del nivel
    private static int nivelActual = 0;
    
    // Funcion actualmente definiendoce
    private static RowTablaSimbolos currentFunction;
    
    // Guarda temporalmente los parametros que se pasan a la
    // funcion
    private static LinkedList<String[]> temporalParameters = new LinkedList();
    
    // Coloca el ultimo valor evaluado de la pila de simbolos
    private static ResPost lastEvaluated;
    
    private static RowTablaSimbolos currentRow;
    public static RowTablaSimbolos currentForRow;
    private static Token currentToken;
    
    public static void startSemantico(){
        tablaDeSimbolos.clear();
    }

    public static void ejecutarRegla(Token token, int numRegla) throws Exception {
        switch (numRegla) {
            case 1:
                // Guardar el tipo de datos
                tipoDeDatoActual = token.getValor();
                break;
            case 2:
                // Verifica que el identificador no este en la tabla de simbolos
                if (tablaDeSimbolos.yaDeclarado(token.getValor(), nivelActual)) {
                    // Error
                    System.out.println("Error Semantico [1]: Identificador "+token.getValor()+" ya declarado");
                } else {
                    // El identificador se registra en la tabla de simbolos
                    RowTablaSimbolos row = new RowTablaSimbolos();
                    currentRow = row;
                    currentToken = token;
                    row.setNivel(nivelActual);
                    row.setIdentificador(token.getValor());
                    row.setTipo(tipoDeDatoActual);
                    String valorDefecto = "";
                    
                    Codigo.elements.add(new CIDeclaracion(tipoDeDatoActual, token.getValor()));
                    
                    switch(tipoDeDatoActual){
                        case "INT":
                            valorDefecto = "0";
                            break;
                        case "DOUBLE":
                            valorDefecto = "0.0";
                            break;
                        case "STRING":
                            valorDefecto = "";
                            break;
                    }
                    row.setValor(valorDefecto);
                    tablaDeSimbolos.add(row);
                    
                    // Si es una funcion la asigna como la actual
                    if(tipoDeDatoActual.equals("funcion")){
                        currentFunction = row;
                    }
                    
                }
                break;
            case 3:
                // Nivel a 0
                nivelActual = 0;
                break;
            case 4:
                // Sube de nivel
                nivelActual++;
                break;
            case 5:
                // Verifica que ya este declarada
                if (!tablaDeSimbolos.yaDeclarado(token.getValor(), nivelActual)) {
                    System.out.println("Error Semantico: Variable no "
                            + "declarada ["+token.getValor()+"] en linea "+ (token.getLinea()+1));
                }else{
                    currentRow = tablaDeSimbolos.buscar(token.getValor(), nivelActual);
                    currentToken = token;
                }
                break;
            case 6:
                // Guarda simbolo en la pila
                //System.out.println("Guarda en pila: "+token.getValor()+" N: "+token.getNumero());
                Postfijo.insertar(token);
                break;
            case 7:
                //System.out.println("Evalua pila");
                lastEvaluated = Postfijo.evaluar(nivelActual);
                if(lastEvaluated!=null){
                    /* System.out.println("RESFINAL: ("
                            +lastEvaluated.getTipo()+") "
                            +lastEvaluated.getValor()); */
                }
                break;
            case 8:
                // Limpia pilas
                Postfijo.limpiar();
                break;
            case 9:
                // Verifica que el identificador no este en la tabla de simbolos
                int nivelAux = nivelActual + 1;
                if (tablaDeSimbolos.yaDeclarado(token.getValor(), nivelAux)) {
                    // Error
                    System.out.println("Error Semantico [1]: Identificador "+token.getValor()+" ya declarado");
                } else {
                    // El identificador se registra en la tabla de simbolos
                    RowTablaSimbolos row = new RowTablaSimbolos();
                    currentRow = row;
                    currentToken = token;
                    row.setNivel(nivelAux);
                    row.setIdentificador(token.getValor());
                    row.setTipo(tipoDeDatoActual);
                    String valorDefecto = "";
                    switch(tipoDeDatoActual){
                        case "INT":
                            valorDefecto = "0";
                            break;
                        case "DOUBLE":
                            valorDefecto = "0.0";
                            break;
                        case "STRING":
                            valorDefecto = "";
                            break;
                    }
                    row.setValor(valorDefecto);
                    tablaDeSimbolos.add(row);
                    
                    // Agrega el parametro a la funcion
                    currentFunction.getParametros().add(new String[]{tipoDeDatoActual , row.getIdentificador()});
                }
                break;
            case 10:
                tipoDeDatoActual = "funcion";
                break;
            case 11:
                // Desactiva los niveles mayores a este
                tablaDeSimbolos.forEach((row)->{
                    if(row.getNivel() > nivelActual){
                        row.setActivo(0);
                    }
                });
                if(Etiquetas.getNivelActual() >= nivelActual){
                    Etiquetas.plachaRestantes();
                }
                break;
            case 12:
                // Verificar que sea una funcion
                if(! currentRow.getTipo().equals("funcion")){
                    System.out.println("Error semantico: el identificador: "
                            +currentRow.getIdentificador()+" no es una funcion");
                }else{
                    // Asigna la funcion actual
                    currentFunction = currentRow;
                }
                break;
            case 13:
                Postfijo.insertar(currentToken);
                break;
            case 14:
                // Limpia la lista de parametros temporales
                temporalParameters.clear();
                break;
            case 15:
                // Guarda el ultimo valor evaluado como un parametro temporal
                temporalParameters.add(new String[]{lastEvaluated.getTipo(),
                    lastEvaluated.getValor()});
                break;
            case 16:
                // Compara la cantidad de parametros que requiere la funcion actual
                // con la cantidad en los temporales
                if (currentFunction.getParametros().size()
                        == temporalParameters.size()) {
                    // Verifica cada tipo contra su definicion
                    // en la funcion
                    int totalParametros = temporalParameters.size();
                    for(int num=0;num<totalParametros; num++){
                        String[] definido = currentFunction.getParametros().get(num);
                        String[] colocado = temporalParameters.get(num);
                        
                        if(! definido[0].equals(colocado[0])){
                            System.out.println("Error semantico:");
                            System.out.println("El parametro de tipo "+colocado[0]);
                            System.out.println("deberia ser un "+definido[0]);
                            break;
                        }
                    }
                }else{
                    System.out.println("Error semantico, la funcion "
                    +currentFunction.getIdentificador()+"\nsolo admite "
                    +currentFunction.getParametros().size()+" parametro(s)\n "
                    +"Se indicaron: "+temporalParameters.size());
                }
                break;
            case 17:
                // Comprueba booleano cuando es un IF
                /*
                    Y genera las etiquets correspondientes
                    para crear los if si se encesitan multiples
                */
                 Etiquetas.generaEtiquetas(nivelActual, RowEtiquetas.IF);
                break;
            case 18:
                // Cuando comeinza un while
                Etiquetas.generaEtiquetas(nivelActual, RowEtiquetas.WHILE);
                Etiquetas.insertaEtiquetaI();
                break;
            case 19:
                // Termino while
                if(lastEvaluated != null && ! lastEvaluated.getTipo().equals("BOOLEAN")){
                    System.out.println("Error semantico:");
                    System.out.println("El valor no es una condicion");
                }else{
                    CondIf.pegarIfs();
                    // CondIfAntiguo.vaciaIf();
                    Etiquetas.insertaEtiquetaV();
                            
                }
                break;
            case 20:
                // Termina un print
                Codigo.elements.add(new CIPrint(Codigo.extract(lastEvaluated.getToken())));
                break;
            case 21:
                // Termina read
                Codigo.elements.add(new CIRead(token.getValor()));
                break;
            case 22:
                /* Asigna el tipo a entero para el for */
                tipoDeDatoActual = "INT";
                break;
            case 23:
                /* Asigna el valor de inicio a la variable del for */
                /* Ademas asigna la variable del for actual */
                currentForRow = currentRow;
                Codigo.elements.add(new CIAsignacionSimple(
                    currentRow.getIdentificador(), Codigo.extract(lastEvaluated.getToken())));
                break;
            case 24:
                /* Crea las etiquetas para un for y pega la etiqueta inicial */
                Etiquetas.generaEtiquetas(nivelActual, RowEtiquetas.FOR);
                Etiquetas.insertaEtiquetaI();
                break;
            case 25:
                /* Crea el if para un for */
                
                String codValue = Codigo.extract(lastEvaluated.getToken());

                Codigo.elements.add(new CIIf(codValue, ">=", currentForRow.getIdentificador(), Etiquetas.getVerdadera()));
                Cuadruple cuadruple = new Cuadruple();
                cuadruple.setOperador("goto");
                cuadruple.setResultado(Etiquetas.getVerdadera()+"");
                Cuadruples.agregarCuadruple(cuadruple);
                
                Codigo.elements.add(new CIGoto(Etiquetas.getFalsa()));
                Etiquetas.insertaEtiquetaV();
                break;
            case 26:
                /* Valida que la sentencia sea un booleano para controles
                de flujo como if y for
                */
                if(lastEvaluated != null && ! lastEvaluated.getTipo().equals("BOOLEAN")){
                    System.out.println("Error semantico:");
                    System.out.println("El valor no es una condicion");
                }else{
                    /* Verifica que no se queden condiciones acumuladas */
                    CondIf.pegarIfs();
                    // CondIfAntiguo.vaciaIf();
                    /* Pega la etiqueta verdadera */
                    Etiquetas.insertaEtiquetaV();
                }
                break;
            default:
                throw new IndexOutOfBoundsException("Regla invalida: " + numRegla);
        }
    }

    public static void imprimirTabla(JTable tablaUi) {
        
        TableColumnModel columnModel =tablaUi.getTableHeader()
                .getColumnModel();
       
        String[] nombreColumnas = {"Nivel", "Tipo", "Id", "Valor", "Activo"};
        
        DefaultTableModel tableModel = new DefaultTableModel(0, 0);
        
        tableModel.setColumnIdentifiers(nombreColumnas);
        
        tablaDeSimbolos.forEach((row) -> {
            
            tableModel.addRow(new Object[]{
                row.getNivel(),
                row.getTipo(),
                row.getIdentificador(),
                row.getValor(),
                row.getActivo()
            });
            
            
            if(! row.getParametros().isEmpty()){
                /* System.out.print(" Parametros: [");
                for(String[] parametro : row.getParametros()){
                    System.out.print("{"+parametro[0]+","+parametro[1]+"}");
                }
                System.out.print("]"); */
            }
        });
        
        tablaUi.setModel(tableModel);
    }
}
