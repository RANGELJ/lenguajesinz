/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lector;

import CI.CIAsignacionDoble;
import CI.Codigo;
import cuadruples.Cuadruple;
import cuadruples.Cuadruples;
import lector.tablaSimbolos.RowTablaSimbolos;
import postfijo.Postfijo;

/**
 *
 * @author mayro
 */
public class Operador {

    public static final int ES_CALCULADO = -2;

    public static TokenV evalua(Token val1, String tipo1, Token op, Token val2, String tipo2,
             int nivel) {
        // Determina el tipo resultado
        try {
            int numTipo1 = toIndice(tipo1);
            int numTipo2 = toIndice(tipo2);
            String tipoRes = tomaMatriz(op.getValor(), op.getNumero())[numTipo1][numTipo2];
            String identificador = crearVariable();
    
            // Toma los valores si son id
            String valor1 = val1.getValor();
            String valor2 = val2.getValor();
            
            String valorCod1=valor1;
            String valorCod2=valor2;
            if(val1 instanceof TokenV){
                valorCod1=((TokenV) val1).getIdentificador();
            }
            if(val2 instanceof TokenV){
                valorCod2=((TokenV) val2).getIdentificador();
            }
            
            /* Colo coloca el intermedio de los que no son relacionales ni logicos */
            if (op.getNumero() == 38 || op.getNumero() == 36) {
                
            }else{
                Codigo.elements.add(new CIAsignacionDoble(
                    identificador, valorCod1, op.getValor(), valorCod2));
            }
            
            
            if (val1.getNumero() == 39) {
                RowTablaSimbolos row = Semantico.tablaDeSimbolos.buscar(val1.getValor(), nivel);
                valor1 = row.getValor();
            }
            if (val2.getNumero() == 39) {
                RowTablaSimbolos row = Semantico.tablaDeSimbolos.buscar(val2.getValor(), nivel);
                valor2 = row.getValor();
            }

            String valor = "0";

            if (!tipoRes.equals("ERROR")) {
                // Si no es error evalua
                switch (op.getNumero()) {
                    case 37:
                        valor = "" + actualOperarArit(valor1, op.getValor(), valor2);
                        break;
                    case 36:
                        valor = "" + actualOperarRelacional(valor1, op.getValor(), valor2);
                        break;
                    case 38:
                        valor = "" + actualOperarLogico(valor1, op.getValor(), valor2);
                        break;
                }
            }
            //System.out.println("Val calc: "+valor);
            if (tipoRes.equals("INT")) {
                valor = "" + ((int) Double.parseDouble(valor));
            }
            
            Cuadruple cuadruple = new Cuadruple();
            cuadruple.setOperando1((val1 instanceof TokenV) ? ((TokenV) val1).getIdentificador() : val1.getValor());
            cuadruple.setOperando2((val2 instanceof TokenV) ? ((TokenV) val2).getIdentificador() : val2.getValor());
            cuadruple.setOperador(op.getValor());
            cuadruple.setResultado(identificador);
            Cuadruples.agregarCuadruple(cuadruple);

            TokenV nuevoTok = new TokenV(valor, ES_CALCULADO, tipoRes, identificador);
            return nuevoTok;
        } catch (NullPointerException ex) {
            return null;
        }
    }

    public static TokenV evaluaNot(Token valor1, int nivel) {
        String tipo = Postfijo.calcularTipo(valor1, nivel);
        if (!tipo.equals("BOOLEAN")) {
            System.out.println("Error, valor no es un booleano: " + valor1.getValor());
            return null;
        } else {
            boolean anterior = Boolean.parseBoolean(valor1.getValor());
            String nombreTemporal = crearVariable();
            TokenV nuevoTok = new TokenV("" + (!anterior), ES_CALCULADO, "BOOLEAN", nombreTemporal);
            Cuadruple nuevoCuadruple = new Cuadruple();
            nuevoCuadruple.setOperando1(valor1.getValor());
            nuevoCuadruple.setOperando1(
                    valor1 instanceof TokenV ?
                            ((TokenV) valor1).getIdentificador() : valor1.getValor());
            nuevoCuadruple.setOperador("NOT");
            nuevoCuadruple.setResultado(nombreTemporal);
            Cuadruples.agregarCuadruple(nuevoCuadruple);
            return nuevoTok;
        }
    }

    private static double actualOperarArit(String val1, String op, String val2) {
        double val1D = Double.parseDouble(val1);
        double val2D = Double.parseDouble(val2);
        switch (op) {
            case "+":
                return val1D + val2D;
            case "-":
                return val1D - val2D;
            case "*":
                return val1D * val2D;
            case "/":
                return val1D / val2D;
        }
        throw new IllegalArgumentException("Operador invalido");
    }

    private static boolean actualOperarRelacional(String val1, String op, String val2) {
        double val1D = Double.parseDouble(val1);
        double val2D = Double.parseDouble(val2);
        switch (op) {
            case "==":
                return val1D == val2D;
            case ">":
                return val1D > val2D;
            case "<":
                return val1D < val2D;
            case ">=":
                return val1D >= val2D;
            case "<=":
                return val1D >= val2D;
        }
        throw new IllegalArgumentException("Operador invalido");
    }

    private static boolean actualOperarLogico(String val1, String op, String val2) {
        boolean val1D = Boolean.parseBoolean(val1);
        boolean val2D = Boolean.parseBoolean(val2);
        switch (op) {
            case "OR":
                return val1D || val2D;
            case "AND":
                return val1D && val2D;
        }
        throw new IllegalArgumentException("Operador invalido");
    }

    private static int ultimaVariable = 0;

    private static String crearVariable() {
        return "Temp" + (ultimaVariable++);
    }

    private static int toIndice(String tipo) {
        switch (tipo) {
            case "INT":
                return 0;
            case "DOUBLE":
                return 1;
            case "STRING":
                return 2;
            case "CHAR":
                return 3;
            case "BOOLEAN":
                return 4;
        }
        return -1;
    }

    private static String[][] tomaMatriz(String operador, int numOp) {
        switch (operador) {
            case "+":
                return matSuma;
            case "-":
            case "*":
                return matRestaMultip;
            case "/":
                return matDivicion;
        }
        switch (numOp) {
            case 36:
                return matRel;
            case 38:
                return matLogico;
        }
        return null;
    }

    private static String[][] matSuma = {
        {"INT", "DOUBLE", "STRING", "ERROR", "INT"},
        {"DOUBLE", "DOUBLE", "STRING", "ERROR", "DOUBLE"},
        {"STRING", "STRING", "STRING", "STRING", "ERROR"},
        {"ERROR", "ERROR", "STRING", "STRING", "ERROR"},
        {"INT", "DOUBLE", "ERROR", "ERROR", "INT"}
    };

    private static String[][] matRestaMultip = {
        {"INT", "DOUBLE", "ERROR", "ERROR", "INT"},
        {"DOUBLE", "DOUBLE", "ERROR", "ERROR", "DOUBLE"},
        {"ERROR", "ERROR", "ERROR", "ERROR", "ERROR"},
        {"ERROR", "ERROR", "ERROR", "ERROR", "ERROR"},
        {"INT", "DOUBLE", "ERROR", "ERROR", "INT"}
    };

    private static String[][] matDivicion = {
        {"DOUBLE", "DOUBLE", "ERROR", "ERROR", "DOUBLE"},
        {"DOUBLE", "DOUBLE", "ERROR", "ERROR", "DOUBLE"},
        {"ERROR", "ERROR", "ERROR", "ERROR", "ERROR"},
        {"ERROR", "ERROR", "ERROR", "ERROR", "ERROR"},
        {"DOUBLE", "DOUBLE", "ERROR", "ERROR", "DOUBLE"}
    };

    private static String[][] matRel = {
        {"BOOLEAN", "BOOLEAN", "ERROR", "ERROR", "BOOLEAN"},
        {"BOOLEAN", "BOOLEAN", "ERROR", "ERROR", "BOOLEAN"},
        {"ERROR", "ERROR", "BOOLEAN", "BOOLEAN", "ERROR"},
        {"ERROR", "ERROR", "BOOLEAN", "BOOLEAN", "ERROR"},
        {"BOOLEAN", "BOOLEAN", "ERROR", "ERROR", "BOOLEAN"}
    };

    private static String[][] matLogico = {
        {"ERROR", "ERROR", "ERROR", "ERROR", "ERROR"},
        {"ERROR", "ERROR", "ERROR", "ERROR", "ERROR"},
        {"ERROR", "ERROR", "ERROR", "ERROR", "ERROR"},
        {"ERROR", "ERROR", "ERROR", "ERROR", "ERROR"},
        {"ERROR", "ERROR", "ERROR", "ERROR", "BOOLEAN"}
    };

}
