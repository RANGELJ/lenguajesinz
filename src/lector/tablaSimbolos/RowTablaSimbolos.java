/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lector.tablaSimbolos;

import java.util.LinkedList;

/**
 *
 * @author
 */
public class RowTablaSimbolos {
    private int nivel;
    private int activo;
    private int usado;
    private String tipo;
    private String identificador;
    private String valor;
    private LinkedList<String[]> parametros;

    public RowTablaSimbolos() {
        activo = 1;
        usado = 0;
        parametros = new LinkedList();
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public void setUsado(int usado) {
        this.usado = usado;
    }

    public int getUsado() {
        return usado;
    }

    public LinkedList<String[]> getParametros() {
        return parametros;
    }
}
