/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lector.tablaSimbolos;

import java.util.LinkedList;

/**
 *
 * @author
 */
public class TablaSimbolos extends LinkedList<RowTablaSimbolos> {

    public boolean yaDeclarado(String identificador, final int nivel) {
        return stream().anyMatch((row)
                -> row.getIdentificador().equals(identificador) && row.getActivo() == 1 && row.getNivel() <= nivel);
    }

    public RowTablaSimbolos buscar(String id,int nivel) {
        try {
            return this.stream().filter((row) -> row.getIdentificador().equals(id)
                    && row.getActivo() == 1 && row.getNivel() <= nivel)
                    .findFirst().get();
        } catch (java.util.NoSuchElementException ex) {
            return null;
        }
    }
}
