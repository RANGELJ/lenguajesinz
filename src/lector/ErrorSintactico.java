/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lector;

/**
 * Excepcion utilizada cuando se rpesenta un error sintactico en la
 * lectura de un codigo fuente.
 * 
 * @author
 */
public class ErrorSintactico extends RuntimeException{
    /* Token en el que se dispara el error */
    private final Token token;

    public ErrorSintactico(Token token, String message) {
        super(message);
        this.token = token;
    }
    
}
