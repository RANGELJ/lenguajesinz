package lector;
%%
%class Lexico
%unicode
%line
%column
%char
%public
%type Token
%function consumirToken
%{
	private Token out(String value, int numero) {
		return new Token(value,numero,yychar,yyline,yycolumn);
	}
%}
%%
// Terminal import
IMPORT			{ return out(yytext(),0);}
// Terminal as
AS			{ return out(yytext(),1);}
// Terminal if
IF			{ return out(yytext(),2);}
// Terminal elif
ELIF			{ return out(yytext(),3);}
// Terminal else
ELSE			{ return out(yytext(),4);}
// Terminal for
FOR			{ return out(yytext(),5);}
// Terminal in
IN			{ return out(yytext(),6);}
// Terminal range
RANGE			{ return out(yytext(),7);}
// Terminal while
WHILE			{ return out(yytext(),8);}
// Terminal def
DEF			{ return out(yytext(),9);}
// Terminal class
CLASS			{ return out(yytext(),10);}
// Terminal print
PRINT			{ return out(yytext(),11);}
// Terminal read
READ			{ return out(yytext(),12);}
// Terminal int
INT			{ return out(yytext(),13);}
// Terminal double
DOUBLE			{ return out(yytext(),14);}
// Terminal string
STRING			{ return out(yytext(),15);}
// Terminal char
CHAR			{ return out(yytext(),16);}
// Terminal boolean
BOOLEAN			{ return out(yytext(),17);}
// Terminal true
TRUE			{ return out(yytext(),18);}
// Terminal false
FALSE			{ return out(yytext(),19);}
// Terminal not
NOT			{ return out(yytext(),20);}
// Terminal tabulador
[\t]			{ return out(yytext(),21);}
// Terminal dosPuntos
[:]			{ return out(yytext(),22);}
// Terminal coma
[,]			{ return out(yytext(),23);}
// Terminal (
[(]			{ return out(yytext(),24);}
// Terminal )
[)]			{ return out(yytext(),25);}
// Terminal asigna
[=]			{ return out(yytext(),26);}
// Terminal punto
\.			{ return out(yytext(),27);}
// Terminal operadorRelacional
==|\!=|>|<|>=|<=			{ return out(yytext(),28);}
// Terminal operadorAritmetico
\+|\-|\*|\/			{ return out(yytext(),29);}
// Terminal operadorLogico
"OR"|"AND"			{ return out(yytext(),30);}
// Terminal identificador
[a-yA-Y][a-yA-Y0-9]*			{ return out(yytext(),31);}
// Terminal entero
[0-9]+			{ return out(yytext(),32);}
// Terminal decimal
[0-9]+\.[0-9]+			{ return out(yytext(),33);}
// Terminal caracter
'([^\n\"])'			{ return out(yytext(),34);}
// Terminal cadena
\"([^\n\"])*\"			{ return out(yytext(),35);}
{##[^\/]\/#}            {}
[^]			{}
