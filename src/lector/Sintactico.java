/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lector;

import lector.listeners.ListenerSintacticoTerminado;
import lector.listeners.ListenerTokenEliminadoDePila;
import lector.listeners.ListenerNoTerminalEliminado;
import lector.listeners.ListenerDerivacion;
import java.io.IOException;
import java.util.LinkedList;

/**
 *
 * @author
 */
public class Sintactico {

    private Lexico lexico;
    /* Esta estructura guarda el estado actual de las derivaciones */
    private final LinkedList<int[]> pila;
    /* Bandera que indica si el proceso de analisis sintactico ya termino */
    private boolean procesoTerminado;
    /* Numero de token para el simbolo delimitador */
    private int NUM_DELIMITADOR = tablaPredictiva[0].length - 1;
    private Token TOKEN_DELIMITADOR = new Token("$", NUM_DELIMITADOR, 0, 0, 0);

    // Listeners de la clase
    private ListenerSintacticoTerminado listenerSintacticoTerminado;
    private ListenerTokenEliminadoDePila listenerTokenEliminadoDePila;
    private ListenerNoTerminalEliminado listenerNoTerminalEliminado;
    private ListenerDerivacion listenerDerivacion;

    public Sintactico(java.io.Reader reader) {
        lexico = new Lexico(reader);
        // Inicializa la pila de derivaciones
        pila = new LinkedList();
        pila.push(new int[]{0, 1});
        procesoTerminado = false;
    }

    /**
     * Realiza el consumo del siguiente terminal en la entrada, si ya no hay
     * ninguno retorna null.
     *
     * Ademas realiza las derivaciones necesarias hasta la eliminacion de dicho
     * terminal
     *
     * @return El ultimo token eliminado del codigo fuente
     */
    public Token consumirTerminal() throws IOException, Exception {
        // Extrae el token
        Token token = lexico.consumirToken();
        // decide si es el ultimo token
        return aplicarSintactico(token);
    }

    private Token aplicarSintactico(Token token) throws Exception {
        if (token != null) {
            // Hace el match entre el tope de la pila
            // el token encontrado
            int[] tope = pila.peekFirst();
            // Verifica que aun se tengan elementos
            if (tope == null) {
                // Ya no hay elementos, el teminal debe ser el
                // de final
                if (token == TOKEN_DELIMITADOR) {
                    // Quita el elemento de la gramatica y termina el proceso
                    procesoTerminado = true;
                    if (listenerSintacticoTerminado != null) {
                        listenerSintacticoTerminado.onAnalisisSintacticoTerminado();
                    }
                    return null;
                } else {
                    throw new ErrorSintactico(token, "No hay mas derivaciones "
                            + "para el token: [ " + nombresTerminales[token.getNumero()] + " ]");
                }
            }

            // Verifica que tipo es este tope
            switch (tope[1]) {
                case 0:
                    // Es un terminal
                    // Compara el resultado con lo el token
                    if (token.getNumero() == tope[0]) {
                        // remueve el tope y retorna el token
                        if (listenerTokenEliminadoDePila != null) {
                            listenerTokenEliminadoDePila.onTokenEliminadoPila(token);
                        }
                        pila.pop();
                        return token;
                    } else {
                        // Error sintactico
                        throw new Error("Error sintactico, tokens no son iguales: "
                                + nombresTerminales[token.getNumero()]
                                + " ::  " + nombresTerminales[tope[0]]);
                    }
                case 1:
                    // Es un no terminal
                    // realiza derivaciones hasta que el elemento
                    // en el tope de la pila es un terminal
                    derivar(token);
                    return aplicarSintactico(token);
                case 2:
                    // Es una regla semantica
                    int numRegla = tope[0];
                    Semantico.ejecutarRegla(token, numRegla);
                    pila.pop();
                    return aplicarSintactico(token);
                default:
                    throw new Error("Tipo de elemento invalido");
            }
        } else {
            // Si no hay mas elementos en la pila
            // el proceso de analisis sintactico a terminado
            if (pila.isEmpty()) {
                procesoTerminado = true;
                if (listenerSintacticoTerminado != null) {
                    listenerSintacticoTerminado.onAnalisisSintacticoTerminado();
                }
                return null;
            } else {
                // Realiza la derivacion de los terminales con el simbolo
                // delimitador

                // Toma el tipo del elemento en la pila
                int tipo = pila.peekFirst()[1];
                switch (tipo) {
                    case 0:
                        // Es un terminal, marca el error
                        throw new Error("Error sintactico, token no encontrado en codigo");
                    case 1:
                        return aplicarSintactico(TOKEN_DELIMITADOR);
                    case 2:
                        // Es una regla semantica
                        Semantico.ejecutarRegla(token, pila.peekFirst()[0]);
                        pila.pop();
                        return aplicarSintactico(TOKEN_DELIMITADOR);
                    default:
                        throw new Error("Tipo de elemento invalido");
                }
            }
        }
    }

    private void derivar(Token token) {
        int numTerminal = token.getNumero();
        int numNoTerminal = pila.peekFirst()[0];
        Integer numeroSigProduccion = tablaPredictiva[numNoTerminal][numTerminal];
        // Verifica si esta derivacion es null
        if (numeroSigProduccion != null) {
            if (listenerDerivacion != null) {
                listenerDerivacion.onDerivacion(numNoTerminal,
                        nombresNoTerminales[numNoTerminal], token,
                        nombresTerminales[numNoTerminal]);
            }
            // Quita el tope
            pila.pop();
            if (listenerNoTerminalEliminado != null) {
                listenerNoTerminalEliminado.onNoTerminalEliminado(numNoTerminal,
                        nombresNoTerminales[numNoTerminal], token,
                        nombresTerminales[numTerminal]);
            }
            // Inserta las derivaciones en la pila
            int[][] produccion = producciones[numeroSigProduccion];
            for (int i = produccion.length - 1; i >= 0; i--) {
                pila.push(produccion[i]);
            }
        } else {
            throw new Error("Error sintactico, "
                    + "token no tiene derivacion\n para el terminal: "
                    + "[" + nombresTerminales[numTerminal] + "] con el "
                    + "no terminal [" + nombresNoTerminales[numNoTerminal] + "]");
        }
    }

    /**
     * Convierte la pila en una cadena legible
     *
     * @return
     */
    public String pilaText() {
        StringBuilder accum = new StringBuilder();
        for (int[] elemento : pila) {
            accum.append("[ ");
            switch (elemento[1]) {
                case 0:
                    accum.append(nombresTerminales[elemento[0]]);
                    break;
                case 1:
                    accum.append(nombresNoTerminales[elemento[0]]);
                    break;
            }
            accum.append(" ]");
        }
        return accum.toString();
    }

    public void addListenerSintacticoTerminado(ListenerSintacticoTerminado listenerSintacticoTerminado) {
        this.listenerSintacticoTerminado = listenerSintacticoTerminado;
    }

    public void addListenerTokenEliminadoDePila(ListenerTokenEliminadoDePila listenerTokenEliminadoDePila) {
        this.listenerTokenEliminadoDePila = listenerTokenEliminadoDePila;
    }

    public void addListenerNoTerminalEliminado(ListenerNoTerminalEliminado listenerNoTerminalEliminado) {
        this.listenerNoTerminalEliminado = listenerNoTerminalEliminado;
    }

    public void addListenerDerivacion(ListenerDerivacion listenerDerivacion) {
        this.listenerDerivacion = listenerDerivacion;
    }

    public String nombreTerminal(int numToken) throws IndexOutOfBoundsException {
        return nombresTerminales[numToken];
    }

    public String nombreNoTerminal(int numNoTerminal) {
        return nombresNoTerminales[numNoTerminal];
    }

    /**
     * Las producciones determinan el flujo de la sintaxis.
     *
     * Las producciones representan sus elementos con un par de numeros con el
     * siguiente formato: {indice , tipo}
     *
     * El tipo puede ser un 0 o un 1 Siendo un 0 es un terminal Siendo un 1 es
     * un NoTerminal
     *
     * El indice indica cual de los elementos del lenguaje es el que se reporta
     *
     */
    private static final int[][][] producciones = {
        //0 	Programa -> Imports Sentencias ;
        {{1, 1}, {2, 1},},
        //1 	Imports -> Import Imports ;
        {{3, 1}, {1, 1},},
        //2 	Imports -> vacio ;
        {},
        //3 	Sentencias -> Anida Sentencia Sentencias ;
        {{3,2} ,{4, 1},{11,2}, {5, 1}, {2, 1},},
        //4 	Sentencias -> vacio ;
        {},
        //5 	Import -> import identificador as identificador ;
        {{0, 0}, {39, 0}, {1, 0}, {39, 0},},
        //6 	Anida -> tabulador Anida ;
        {{4,2}, {27, 0}, {4, 1},},
        //7 	Anida -> vacio ;
        {},
        //8 	Sentencia -> If ;
        {{6, 1},},
        //9 	Sentencia -> Elif ;
        {{7, 1},},
        //10 	Sentencia -> Else ;
        {{8, 1},},
        //11 	Sentencia -> For ;
        {{9, 1},},
        //12 	Sentencia -> While ;
        {{10, 1},},
        //13 	Sentencia -> Imprimir ;
        {{11, 1},},
        //14 	Sentencia -> Leer ;
        {{12, 1},},
        //15 	Sentencia -> DefineFuncion ;
        {{13, 1},},
        //16 	Sentencia -> Declaracion ;
        {{14, 1},},
        //17 	Sentencia -> AuxSentencia ;
        {{15, 1},},
        //18 	Sentencia -> break ;
        {{21, 0},},
        //19 	Sentencia -> continue ;
        {{22, 0},},
        //20 	Sentencia -> ReturnSt ;
        {{16, 1},},
        //21 	Sentencia -> try dosPuntos ;
        {{24, 0}, {28, 0},},
        //22 	Sentencia -> except identificador dosPuntos ;
        {{25, 0}, {39, 0}, {28, 0},},
        //23 	Sentencia -> finally dosPuntos ;
        {{26, 0}, {28, 0},},
        //24 	If -> if Valor dosPuntos ;
        {{2, 0}, {17, 1}, {17,2}, {7,2}, {26,2},{28, 0},},
        //25 	Elif -> elif Valor dosPuntos ;
        {{3, 0}, {17, 1}, {7,2}, {28, 0},},
        //26 	Else -> else dosPuntos ;
        {{4, 0}, {28, 0},},
        //27 	For -> for identificador in range ( Valor coma Valor ) dosPuntos ;
        {{5, 0},{22,2}, {2,2}, {39, 0}, {6, 0}, {7, 0}, {30, 0}, {17, 1}, {7,2},{23,2},{24,2}, {29, 0}, {17, 1}, {7,2}, {25,2}, {31, 0}, {28, 0},},
        //28 	While -> while Valor dosPuntos ;
        {{18,2}, {8, 0}, {17, 1}, {7,2}, {19,2}, {28, 0},},
        //29 	Imprimir -> print ( Valor ) ;
        {{11, 0}, {30, 0}, {17, 1}, {7,2}, {20,2}, {31, 0},},
        //30 	Leer -> read ( identificador ) ;
        {{12, 0}, {30, 0},{2,2}, {21,2}, {39, 0}, {31, 0},},
        //31 	DefineFuncion -> def identificador ( DefParams ) dosPuntos ;
        {{10,2},{9, 0},{2,2}, {39, 0}, {30, 0}, {18, 1}, {31, 0}, {28, 0},},
        //32 	Declaracion -> Tipodato identificador Asignacion ;
        {{1,2},{19, 1},{2,2},{6,2}, {39, 0}, {20, 1},{7,2}},
        //33 	AuxSentencia -> identificador AuxSentencia2 ;
        {{5,2},{39, 0}, {21, 1},},
        //34 	ReturnSt -> return Valor ;
        {{23, 0}, {17, 1}, {7,2}},
        //35 	Valor -> ValorB Valor2 ;
        {{22, 1}, {23, 1},},
        //36 	DefParams -> Tipodato identificador DefParams2 ;
        {{1,2},{19, 1},{9,2},{39, 0}, {24, 1},},
        //37 	DefParams -> vacio ;
        {},
        //38 	Tipodato -> int ;
        {{13, 0},},
        //39 	Tipodato -> double ;
        {{14, 0},},
        //40 	Tipodato -> string ;
        {{15, 0},},
        //41 	Tipodato -> char ;
        {{16, 0},},
        //42 	Tipodato -> boolean ;
        {{17, 0},},
        //43 	Asignacion -> asigna Valor ;
        {{6,2},{32, 0}, {17, 1}, {7,2}},
        //44 	Asignacion -> vacio ;
        {},
        //45 	AuxSentencia2 -> ( ParametrosPas ) ;
        {{12,2},{14,2},{30, 0}, {25, 1}, {31, 0},{16,2}},
        //46 	AuxSentencia2 -> asigna Valor ;
        {{13,2},{6,2},{32, 0}, {17, 1},{7,2}},
        //47 	ValorB -> ( Valor ) ;
        {{6,2},{30, 0}, {17, 1}, {6,2},{31, 0},},
        //48 	ValorB -> entero ;
        {{6,2},{40, 0},},
        //49 	ValorB -> identificador ;
        {{5,2},{6,2},{39, 0}},
        //50 	ValorB -> decimal ;
        {{6,2},{41, 0},},
        //51 	ValorB -> cadena ;
        {{6,2},{43, 0},},
        //52 	ValorB -> caracter ;
        {{6,2},{42, 0},},
        //53 	ValorB -> masmas Valor ;
        {{6,2},{34, 0}, {17, 1},},
        //54 	ValorB -> menosmenos Valor ;
        {{6,2},{35, 0}, {17, 1},},
        //55 	ValorB -> not Valor ;
        {{6,2},{20, 0}, {17, 1},},
        //56 	Valor2 -> operadorAritmetico Valor Valor2 ;
        {{6,2},{37, 0}, {17, 1}, {23, 1},},
        //57 	Valor2 -> operadorRelacional Valor Valor2 ;
        {{6,2},{36, 0}, {17, 1}, {23, 1},},
        //58 	Valor2 -> operadorLogico Valor Valor2 ;
        {{6,2},{38, 0}, {17, 1}, {23, 1},},
        //59 	Valor2 -> vacio ;
        {},
        //60 	DefParams2 -> coma Tipodato identificador DefParams2 ;
        {{29, 0}, {1,2},{19, 1}, {9,2},{39, 0},{24, 1}},
        //61 	DefParams2 -> vacio ;
        {},
        //62 	ParametrosPas -> Valor ParametrosPas2 ;
        {{17, 1}, {7,2},{15,2}, {26, 1},},
        //2 	ParametrosPas -> vacio ;
        {},
        //64 	ParametrosPas2 -> coma ParametrosPas ;
        {{29, 0}, {25, 1},},
        //2 	ParametrosPas2 -> vacio ;
        {},
        //66 	ParamsAux -> ( ParametrosPas ) ;
        {{30, 0}, {25, 1}, {31, 0},},
        //2 	ParamsAux -> vacio ;
        {},};

    public static final String[] nombresNoTerminales = {"Programa", "Imports", "Sentencias", "Import", "Anida", "Sentencia", "If", "Elif", "Else", "For", "While", "Imprimir", "Leer", "DefineFuncion", "Declaracion", "AuxSentencia", "ReturnSt", "Valor", "DefParams", "Tipodato", "Asignacion", "AuxSentencia2", "ValorB", "Valor2", "DefParams2", "ParametrosPas", "ParametrosPas2", "ParamsAux"};

    public static final String[] nombresTerminales = {"import", "as", "if", "elif", "else", "for", "in", "range", "while", "def", "class", "print", "read", "int", "double", "string", "char", "boolean", "true", "false", "not", "break", "continue", "return", "try", "except", "finally", "tabulador", "dosPuntos", "coma", "(", ")", "asigna", "punto", "masmas", "menosmenos", "operadorRelacional", "operadorAritmetico", "operadorLogico", "identificador", "entero", "decimal", "caracter", "cadena", "$$"};


    /*
        Falta comprobacion en funciones y evaluacion en pila de metodos
     */
    /**
     * La tabla predictiva guia el proceso de analisis sintactico.
     *
     * Cada columna es un no terminal de la gramatica Cada fila es un terminal
     * del lenguaje
     *
     * El contenido de cada direccion es el numero de una produccion a derivar o
     * null si no se tiene ninguna derivacion para una convinacion dada de token
     * y noTerminal
     */
    private static final Integer[][] tablaPredictiva = {
        {0, null, 0, 0, 0, 0, null, null, 0, 0, null, 0, 0, 0, 0, 0, 0, 0, null, null, null, 0, 0, 0, 0, 0, 0, 0, null, null, null, null, null, null, null, null, null, null, null, 0, null, null, null, null, null,},
        {1, null, 2, 2, 2, 2, null, null, 2, 2, null, 2, 2, 2, 2, 2, 2, 2, null, null, null, 2, 2, 2, 2, 2, 2, 2, null, null, null, null, null, null, null, null, null, null, null, 2, null, null, null, null, 2,},
        {null, null, 3, 3, 3, 3, null, null, 3, 3, null, 3, 3, 3, 3, 3, 3, 3, null, null, null, 3, 3, 3, 3, 3, 3, 3, null, null, null, null, null, null, null, null, null, null, null, 3, null, null, null, null, 4,},
        /*3*/{5, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,},
        {null, null, 7, 7, 7, 7, null, null, 7, 7, null, 7, 7, 7, 7, 7, 7, 7, null, null, null, 7, 7, 7, 7, 7, 7, 6, null, null, null, null, null, null, null, null, null, null, null, 7, null, null, null, null, null,},
        {null, null, 8, 9, 10, 11, null, null, 12, 15, null, 13, 14, 16, 16, 16, 16, 16, null, null, null, 18, 19, 20, 21, 22, 23, null, null, null, null, null, null, null, null, null, null, null, null, 17, null, null, null, null, null,},
        {null, null, 24, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,},
        /*7*/{null, null, null, 25, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,},
        {null, null, null, null, 26, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,},
        {null, null, null, null, null, 27, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,},
        {null, null, null, null, null, null, null, null, 28, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,},
        /*11*/{null, null, null, null, null, null, null, null, null, null, null, 29, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,},
        {null, null, null, null, null, null, null, null, null, null, null, null, 30, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,},
        {null, null, null, null, null, null, null, null, null, 31, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,},
        {null, null, null, null, null, null, null, null, null, null, null, null, null, 32, 32, 32, 32, 32, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,},
        {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 33, null, null, null, null, null,},
        {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 34, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,},
        {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 35, null, null, null, null, null, null, null, null, null, 35, null, null, null, 35, 35, null, null, null, 35, 35, 35, 35, 35, null,},
        /*18*/{null, null, null, null, null, null, null, null, null, null, null, null, /*12*/null, 36, 36, 36, 36, /*17*/36, null, null, null, null, null, null, /*24*/null, null, null, null, null, /*29*/null, null, /*31*/37, null, null, null, null, null, null, null, null, null, null, null, null, null,},
        {null, null, null, null, null, null, null, null, null, null, null, null, null, 38, 39, 40, 41, 42, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,},
        {null, null, 44, 44, 44, 44, null, null, 44, 44, null, 44, 44, 44, 44, 44, 44, 44, null, null, null, 44, 44, 44, 44, 44, 44, 44, null, null, null, null, 43, null, null, null, null, null, null, 44, null, null, null, null, 44,},
        {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 45, null, 46, null, null, null, null, null, null, null, null, null, null, null, null,},
        {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 55, null, null, null, null, null, null, null, null, null, 47, null, null, null, 53, 54, null, null, null, 49, 48, 50, 52, 51, null,},
        {null, null, 59, 59, 59, 59, null, null, 59, 59, null, 59, 59, 59, 59, 59, 59, 59, null, null, null, 59, 59, 59, 59, 59, 59, 59, 59, 59, null, 59, null, null, null, null, 57, 56, 58, 59, null, null, null, null, 59,},
        /*24*/{null, null, null, null, null, null, null, null, null, null, null, null, null, /*14*/null, null, null, null, null, null, null, null, /*22*/null, null, null, null, null, null, null, null, /*29*/60,null, 61, null, null, null, null, null, null, null, null, null, null, null, null, null,},
        {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 62, null, null, null, null, null, null, null, null, null, 62, 63, null, null, 62, 62, null, null, null, 62, 62, 62, 62, 62, null,},
        {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 64, null, 65, null, null, null, null, null, null, null, null, null, null, null, null, null,},
        {null, null, 67, 67, 67, 67, null, null, 67, 67, null, 67, 67, 67, 67, 67, 67, 67, null, null, null, 67, 67, 67, 67, 67, 67, 67, 67, 67, 66, 67, null, null, null, null, 67, 67, 67, 67, null, null, null, null, 67,},};
}
