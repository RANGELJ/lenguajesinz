package lector;

/**
 * Unidad del lenguaje en el texto
 * 
 * @author
 */
public class Token {
    
    private final String valor;
    private final int posicion;
    private final int numero;
    private final int linea;
    private final int columna;

    public Token(String valor, int numero, int posicion, int linea, int columna) {
        this.valor = valor;
        this.posicion = posicion;
        this.numero = numero;
        this.linea = linea;
        this.columna = columna;
    }

    public String getValor() {
        return valor;
    }
    
    /**
     * @return Numero de caracter en donde comienza este token
     */
    public int getPosicion() {
        return posicion;
    }

    public int getNumero() {
        return numero;
    }

    public int getLinea() {
        return linea;
    }

    public int getColumna() {
        return columna;
    }
}
