/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lector.listeners;

import java.util.EventListener;
import lector.Token;

/**
 * Se llama cuando hay una derivacion en el proceso sintactico
 *
 * @author 
 */
public interface ListenerDerivacion extends EventListener {

    public void onDerivacion(int numNoTerminal,
            String nombreNoTerminal, Token token,
            String nombreToken);
}
