/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lector.listeners;

import java.util.EventListener;

/**
 * Se activa cuando el analisis sintactico termina
 * de forma exitosa.
 * 
 * @author 
 */
public interface ListenerSintacticoTerminado extends EventListener{
    public abstract void onAnalisisSintacticoTerminado();
}
