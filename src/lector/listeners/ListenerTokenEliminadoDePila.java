/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lector.listeners;

import java.util.EventListener;
import lector.Token;

/**
 *
 * @author 
 */
public interface ListenerTokenEliminadoDePila extends EventListener{
    public void onTokenEliminadoPila(Token token);
}
