/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package postfijo;

import CI.CIAsignacionSimple;
import CI.Codigo;
import CI.Condiciones.CondIf;
import cuadruples.Cuadruple;
import cuadruples.Cuadruples;
import java.util.LinkedList;
import javax.swing.JTextArea;
import lector.Operador;
import lector.Semantico;
import lector.Token;
import lector.TokenV;
import lector.tablaSimbolos.RowTablaSimbolos;

/**
 *
 * @author mayro
 */
public class Postfijo {

    private static final LinkedList<Token> PILA_IZQUIERDA = new LinkedList();
    private static final LinkedList<TokenOp> PILA_DERECHA = new LinkedList();
    public static JTextArea areaPila;

    public static void insertar(Token token) {
        if (esValor(token.getNumero())) {
            PILA_IZQUIERDA.push(token);
        } else if (esOperador(token.getNumero())) {
            // Operador o parentesis que abre
            // Sacar prioridad
            int prioridad = getPrioridad(token.getValor());
            insertaOperador(token, prioridad);
        } else if (token.getNumero() == 30) {
            // Parentesis que abre
            int prioridad = getPrioridad(token.getValor());
            TokenOp tokenOp = new TokenOp(token.getValor(), token.getNumero(),
                    token.getPosicion(), token.getLinea(), token.getColumna());
            PILA_DERECHA.push(tokenOp);
        } else if (token.getNumero() == 31) {
            // Parentesis que cierra
            // Saca operadores hasta encontrar su pareja
            while (!PILA_DERECHA.isEmpty()) {
                if (PILA_DERECHA.peekFirst().getNumero() == 30) {
                    // Encontro su par
                    // Lo saca y desecha los parentesis
                    PILA_DERECHA.pop();
                    break;
                } else {
                    // Inserta en la izquierda
                    PILA_IZQUIERDA.push(PILA_DERECHA.pop());
                }
            }
        }
        //imprimirEstado();
        //System.out.println("###########################################");
    }

    public static ResPost evaluar(int nivel) throws Exception {
        // Pasa todo a la izquierda
        while (!PILA_DERECHA.isEmpty()) {
            PILA_IZQUIERDA.push(PILA_DERECHA.pop());
        }

        imprimirEstado();
        CondIf.iniciarGeneradorCondiciones();

        // Pila auxiliar de operaciones
        LinkedList<Token> PILA_AUX = new LinkedList();

        // Si solo se trata de un elemento no evalua
        if(PILA_IZQUIERDA.size() == 1){
            Token unique = PILA_IZQUIERDA.pop();
            String valor = unique.getValor();
            if(unique.getNumero() == 39){
                RowTablaSimbolos row = Semantico.tablaDeSimbolos.buscar(valor, nivel);
                if (row == null) {
                    System.out.println("Error por variable no declarada");
                    return null;
                }else{
                    valor = row.getValor();
                }
            }
            ResPost resP = new ResPost(calcularTipo(unique, nivel), valor, unique);
            return resP;
        }
        
        TokenV res = null;
        while (!PILA_IZQUIERDA.isEmpty()) {
            Token ultimo = PILA_IZQUIERDA.pollLast();
            if (esValor(ultimo.getNumero())) {
                PILA_AUX.push(ultimo);
            } else {
                // Es un operador
                Token operador = ultimo;
                // Verifica si es un operador unitario
                if (operador.getNumero() == 20) {
                    CondIf.insertaOperadorLogico(operador.getValor());
                    Token valor1 = PILA_AUX.pop();
                    res = Operador.evaluaNot(valor1, nivel);
                    PILA_AUX.push(res);
                } else {
                    Token valor2 = PILA_AUX.pop();
                    Token valor1 = PILA_AUX.pop();

                    // Toma los tipos de cada valor
                    String tipo1 = calcularTipo(valor1, nivel);
                    String tipo2 = calcularTipo(valor2, nivel);

                    if (operador.getNumero() == 32) {
                        // asignacion
                        // System.out.println("Asigna: " + valor1.getValor() + " " + operador.getValor() + " " + valor2.getValor());
                        RowTablaSimbolos row = Semantico.tablaDeSimbolos.buscar(valor1.getValor(), nivel);
                        if(row != null){
                            Cuadruple cuadruple = new Cuadruple();
                            cuadruple.setOperando1(valor2 instanceof TokenV ? ((TokenV)valor2).getIdentificador() : valor2.getValor());
                            cuadruple.setOperador(operador.getValor());
                            cuadruple.setResultado(row.getIdentificador());
                            Cuadruples.agregarCuadruple(cuadruple);
                            
                            row.setValor(valor2.getValor());
                            
                            String valorCod2=Codigo.extract(valor2);
                            
                            Codigo.elements.add(new CIAsignacionSimple(row.getIdentificador(),
                                valorCod2));
                            //Semantico.imprimirTabla();
                        }else{
                            System.out.println("Asignacion invalida, variable "+valor1.getValor()+" no declarada");
                        }
                    } else {
                        
                        /*
                            Verifica si se trata de un operado logico valido.
                        */
                        if(operador.getNumero() == 38){
                            /* operador logico */
                            CondIf.insertaOperadorLogico(operador.getValor());
                            // CondIfAntiguo.acumularOperadorLog(operador.getValor());
                        }else if(operador.getNumero() == 36){
                            /* Operador relacional */
                            CondIf.registrarIf(valor1.getValor(), operador.getValor(), valor2.getValor());
                            // CondIfAntiguo.acumulaIf(valor1, operador.getValor(), valor2);
                            // System.out.println("IF "+valor1.getValor()+operador.getValor()+valor2.getValor());
                        }
                         
                        //System.out.println("OPERA: (" + tipo1 + ") " + valor1.getValor()
                          //      + " " + operador.getValor() + " (" + tipo2 + ") " + valor2.getValor());

                        res = Operador.evalua(valor1, tipo1, operador, valor2, tipo2, nivel);
                        //System.out.println(" Resultado: "+res);
                        if (res == null) {
                            // No sigue operando la exprecion
                            System.out.println("Error por variable no definida");
                            limpiar();
                            return null;
                        }
                        PILA_AUX.push(res);
                    }
                }

            }
        }
        
        limpiar();
        if(res != null){
            return new ResPost(res.getTipo(), res.getValor(), res);
        }else{
            return null;
        }
        
    }

    public static String calcularTipo(Token token, int nivel) {
        if (token instanceof TokenV) {
            // Se trata de una variable calculada
            return ((TokenV) token).getTipo();
        }
        if (token.getNumero() == 39) {
            RowTablaSimbolos row = Semantico.tablaDeSimbolos.buscar(token.getValor(), nivel);
            if (row != null) {
                return row.getTipo();
            } else {
                return null;
            }
        } else {
            return getTipo(token.getNumero());
        }
    }

    private static String getTipo(int num) {
        switch (num) {
            case 40: // entero
                return "INT";
            case 41: // decimal
                return "DOUBLE";
            case 43: // cadena
                return "STRING";
            case 42: // caracter
                return "CHAR";
        }
        return null;
    }

    public static void limpiar() {
        PILA_DERECHA.clear();
        PILA_IZQUIERDA.clear();
    }

    private static void insertaOperador(Token token, int prioridad) {
        TokenOp tokenOp = new TokenOp(token.getValor(), token.getNumero(),
                token.getPosicion(), token.getLinea(), token.getColumna());
        tokenOp.setPrioridad(prioridad);
        // casos
        if (PILA_DERECHA.isEmpty() || PILA_DERECHA.peekFirst().getNumero() == 30) {
            // Parentesis que abre
            PILA_DERECHA.push(tokenOp);
        } else {
            // toma la ultima prioridad
            int ultimaPrior = PILA_DERECHA.peekFirst().getPrioridad();
            if (ultimaPrior > prioridad) {
                // entra en la pila
                PILA_DERECHA.push(tokenOp);
            } else {
                // Saca el operador y vuelve a verificar
                PILA_IZQUIERDA.push(PILA_DERECHA.pop());
                insertaOperador(token, prioridad);
            }
        }
    }

    private static int getPrioridad(String valor) {
        int prioridad = -1;
        for (int prior = 0; prior < ordenOperadores.length; prior++) {
            String[] arregloN = ordenOperadores[prior];
            // Busca el token
            boolean encontrado = false;
            for (String comparador : arregloN) {
                if (comparador.equals(valor)) {
                    encontrado = true;
                    break;
                }
            }
            if (encontrado) {
                // Este es el nivel
                prioridad = prior;
                break;
            }
        }
        return prioridad;
    }

    private static boolean esValor(int numToken) {
        switch (numToken) {
            case 40: // entero
            case 41: // decimal
            case 43: // cadena
            case 42: // caracter
            case 39: // identificador
                return true;
        }
        return false;
    }

    private static boolean esOperador(int numToken) {
        switch (numToken) {
            case 37: // oA
            case 36: // oR
            case 38: // oL (Sin not)
            case 20: // NOT
            case 32: // Asignacion =
                return true;
        }
        return false;
    }

    private static String[][] ordenOperadores = {
        {"*", "/"},
        {"+", "-"},
        {"%"},
        {"==", "=/=", "<", ">", "<=", ">="},
        {"NOT"},
        {"OR", "AND"},
        {"="}
    };

    public static void imprimirEstado() {
        int numElentos = (PILA_DERECHA.size() > PILA_IZQUIERDA.size())
                ? PILA_DERECHA.size() : PILA_IZQUIERDA.size();

        for (int i = 0; i <= numElentos; i++) {
            String valorDer = "";
            String valorIz = "";
            try {
                valorDer = PILA_DERECHA.get(i).getValor();
            } catch (IndexOutOfBoundsException e) {
            }
            try {
                valorIz = PILA_IZQUIERDA.get(i).getValor();
            } catch (IndexOutOfBoundsException e) {
            }

            areaPila.append("| " + valorIz + " |\t| " + valorDer + " |\n");
        }
        
        areaPila.append("###############################\n");
    }

    private static class TokenOp extends Token {

        private int prioridad;

        public TokenOp(String valor, int numero, int posicion, int linea, int columna) {
            super(valor, numero, posicion, linea, columna);
        }

        public int getPrioridad() {
            return prioridad;
        }

        public void setPrioridad(int prioridad) {
            this.prioridad = prioridad;
        }

    }
}
