/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package postfijo;

import lector.Token;

/**
 *
 * @author anna Peralta Jorge Luis
 */
public class ResPost {
    private final String tipo;
    private final String valor;
    private final Token token;

    public ResPost(String tipo, String valor, Token token) {
        this.tipo = tipo;
        this.valor = valor;
        this.token = token;
    }

    public String getTipo() {
        return tipo;
    }

    public String getValor() {
        return valor;
    }

    public Token getToken() {
        return token;
    }
    
}
