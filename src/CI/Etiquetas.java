/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CI;

import CI.Condiciones.CondIfAntiguo;
import cuadruples.Cuadruple;
import cuadruples.Cuadruples;
import java.util.LinkedList;
import lector.Semantico;

/**
 *
 * @author mayro
 */
public class Etiquetas {
    private static LinkedList<RowEtiquetas> pila = new LinkedList();
    private static int numAct=0;
    
    public static int nueva(){
        return numAct++;
    }
    
    public static void reiniciaEtiquetas(){
        numAct = 0;
    }
    
    public static void limpiar(){
        pila.clear();
        reiniciaEtiquetas();
    }
    public static void generaEtiquetas(int nivel, int tipo){
        /* Verdadera, falsa, inicial */
        int nuevaV = nueva();
        int nuevaF = nueva();
        int nuevaI = nueva();
        pila.push(new RowEtiquetas(nuevaV, nuevaF, nuevaI, nueva(), nivel, tipo));
        /* Inicializa el consructor de los if */
        CondIfAntiguo.iniciar(nuevaV, nuevaF, nuevaI);
    }
    public static void insertaEtiquetaV(){
        Codigo.elements.add(new CIEtiqueta(getVerdadera()));
    }
    public static void insertaEtiquetaF(){
        Codigo.elements.add(new CIEtiqueta(getFalsa()));
    }
    public static void insertaEtiquetaI(){
        Codigo.elements.add(new CIEtiqueta(getInicial()));
    }
    public static void plachaRestantes(){
        while(!pila.isEmpty()){
            RowEtiquetas currRow = pila.getFirst();
            switch(currRow.getTipo()){
                case RowEtiquetas.IF:
                    insertaEtiquetaF();
                    break;
                case RowEtiquetas.WHILE:
                    Codigo.elements.add(new CIGoto(getInicial()));
                    Cuadruple cuadruple = new Cuadruple();
                    cuadruple.setOperador("goto");
                    cuadruple.setResultado(getInicial()+"");
                    Cuadruples.agregarCuadruple(cuadruple);
                    insertaEtiquetaF();
                    break;
                case RowEtiquetas.FOR:
                    /* Increment */
                    String identificadorFor = Semantico.currentForRow.getIdentificador();
                    Codigo.elements.add(new CIAsignacionDoble(
                        identificadorFor, identificadorFor, "+", "1"));
                    Codigo.elements.add(new CIGoto(getInicial()));
                    Etiquetas.insertaEtiquetaF();
                    break;
            }
            pila.pop();
        }
    }
    public static int getNivelActual(){
        if(!pila.isEmpty()){
            return pila.getFirst().getNivel();
        }else{
            return -1;
        }
    }
    public static void deleteCurrentRow(){
        pila.pop();
    }
    
    public static int getVerdadera(){
        return pila.getFirst().getVerdadera();
    }
    
    public static int getFalsa(){
        return pila.getFirst().getFalsa();
    }
    public static int getInicial(){
        return pila.getFirst().getInicial();
    }
    
    public static int getAuxiliar() {
        return pila.getFirst().getAuxiliar();
    }
    
    
}
