/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CI;

/**
 *
 * @author anna
 */
public class CIPrint extends CIElement {
    
    private final String apuntador;

    public CIPrint(String apuntador) {
        this.apuntador = apuntador;
    }

    @Override
    public void addToEnsamblador(StringBuilder codigo, StringBuilder data) {

        String apuntador = this.apuntador;
        if (this.esUnaCadenaLiteral(this.apuntador)) {
            int nuevaEtiqueta = Etiquetas.nueva();
            apuntador = "ms"+nuevaEtiqueta;
            data.append(apuntador).append(" db 10,13,").append(this.apuntador)
                .append(",'$'\n");
            codigo.append(";Interrupcion de imprimir cadena\n");
            codigo.append("MOV AH,09h\n");
            codigo.append("LEA DX,"+apuntador).append("\n");
            codigo.append("INT 21h\n");
            this.addSaltoDeLineaEnConsola(codigo);
        } else {
            codigo.append(";Imprimir numero\n");
            codigo.append("MOV AX,"+this.apuntador+"\n");
            codigo.append("call PRINT_NUM\n");
            this.addSaltoDeLineaEnConsola(codigo);
        }
        codigo.append(";---------------------------\n");
        return;
    }
    
    @Override
    public String getRaw() {
        return "print " + this.apuntador;
    }

}
