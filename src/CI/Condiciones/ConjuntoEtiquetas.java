/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CI.Condiciones;

/**
 *
 * @author anna
 */
public class ConjuntoEtiquetas {
    public final Integer verdadera;
    public final Integer falsa;
    public final Integer auxiliar;

    public ConjuntoEtiquetas(Integer verdadera, Integer falsa, Integer auxiliar) {
        this.verdadera = verdadera;
        this.falsa = falsa;
        this.auxiliar = auxiliar;
    }    
    
}
