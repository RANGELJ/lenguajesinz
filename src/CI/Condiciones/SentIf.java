/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CI.Condiciones;

/**
 * Representa una sentencia if con todos sus componentes
 * para el codigo intemedio
 * @author mayros
 */
public class SentIf {
    
    private final String operando1;
    private final String operando2;
    private final String operador;
    
    private int etiquetaVerdadera;
    private int etiquetaFalse;
    private Integer etiquetaAuxiliar;

    public SentIf(String operando1, String operador, String operando2) {
        this.operando1 = operando1;
        this.operando2 = operando2;
        this.operador = operador;
        this.etiquetaAuxiliar = null;
    }

    public String getOperando1() {
        return operando1;
    }

    public String getOperando2() {
        return operando2;
    }

    public String getOperador() {
        return operador;
    }

    public void setEtiquetaVerdadera(int etiquetaVerdadera) {
        this.etiquetaVerdadera = etiquetaVerdadera;
    }

    public void setEtiquetaFalse(int etiquetaFalse) {
        this.etiquetaFalse = etiquetaFalse;
    }

    public void setEtiquetaAuxiliar(int etiquetaAuxiliar) {
        this.etiquetaAuxiliar = etiquetaAuxiliar;
    }
    
    @Override
    public String toString(){
        
        String codigo = "if "+operando1+" "+operador+" "+operando2
                +" goto "+etiquetaVerdadera+"\n"
                +"goto "+etiquetaFalse+"\n";
        
        if (operando1 == null && operador == null && operando2 == null) {
            codigo = "";
        }
        
        if (this.etiquetaAuxiliar != null) {
            codigo += this.etiquetaAuxiliar+":\n";
        }
        return codigo;
    }
}
