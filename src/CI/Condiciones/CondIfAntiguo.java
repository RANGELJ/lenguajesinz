/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CI.Condiciones;

import CI.Codigo;
import CI.Etiquetas;
import lector.Token;

/**
 * Para crear las etiquetas y el codigo intermedio en condiciones de peradores
 * relacionales
 *
 * @author mayro
 */
public class CondIfAntiguo {

    private static int etiquetaInicial;
    private static int etiquetaVerdadera;
    private static int etiquetaFalsa;

    private static SentIf ifAux1;
    private static SentIf ifAux2;
    private static String operadorLogico;

    public static void iniciar(int ev, int ef, int ei) {

        /* Crea las etiquetas globales */
        etiquetaVerdadera = ev;
        etiquetaFalsa = ef;
        etiquetaInicial = ei;
    }

    /* Cuando se trata de una condicion sin operadores relacionales
        dicha condicion queda aqui y es sacada
     */
    public static void vaciaIf() {
        if (ifAux1 != null) {
            ifAux1.setEtiquetaVerdadera(etiquetaVerdadera);
            ifAux1.setEtiquetaFalse(etiquetaFalsa);
            //Codigo.acum.append(ifAux1);
            ifAux1 = null;
        }
    }

    /**
     * Crea el if a partir de un operador relacional y sus operandos
     */
    public static void acumulaIf(Token tref1, String opRel, Token tref2) {

        String ref1 = Codigo.extract(tref1);
        String ref2 = Codigo.extract(tref2);

        if (ifAux1 == null) {
            /* Queda en espera del siguiente if para construir la estructura */
            ifAux1 = new SentIf(ref1, opRel, ref2);
        } else {
            /* Crea el segundo if para la acumulacion */
            ifAux2 = new SentIf(ref1, opRel, ref2);
        }
    }

    public static void acumularOperadorLog(String operador) {
        operadorLogico = operador;
        /* Comprueba el tipo de operador para crear las etiquetas a corde */
        switch (operadorLogico) {
            case "OR":
                /*
                        Si el primero es verdadero se va a verdadero principal
                        saltandose la segunda condicion
                 */
                ifAux1.setEtiquetaVerdadera(etiquetaVerdadera);
                /*
                        Si el primero es falso verifica la condicion 2
                        para lo cual se crea una etiqueta intermedia
                 */
                int etiquetaIntermedia = Etiquetas.nueva();
                ifAux1.setEtiquetaFalse(etiquetaIntermedia);
                /* Si la condicion 2 es verdadera se va a la verdadera principal */
                ifAux2.setEtiquetaVerdadera(etiquetaVerdadera);
                /*
                        Si la condicion 2 es falsa se va al false principal
                 */
                ifAux2.setEtiquetaFalse(etiquetaFalsa);
                /*
                        Pega las condiciones
                 */
                //Codigo.acum.append(ifAux1);
                //Codigo.acum.append(":").append(etiquetaIntermedia).append("\n");
                //Codigo.acum.append(ifAux2);
                break;
            case "AND":
                /*
                    Si el primero es verdadero va a verificar el segundo
                    si no es verdadero sale de la condicion
                */
                int etiquetaAux = Etiquetas.nueva();
                ifAux1.setEtiquetaVerdadera(etiquetaAux);
                ifAux1.setEtiquetaFalse(etiquetaFalsa);
                /*
                    Si la segunda condicion es verdadera, se va a verdadero
                    , si es falsa se va a falso
                */
                ifAux2.setEtiquetaFalse(etiquetaFalsa);
                ifAux2.setEtiquetaVerdadera(etiquetaVerdadera);
                /*
                    Plancha las condiciones
                */
                //Codigo.acum.append(ifAux1);
                //Codigo.acum.append(":").append(etiquetaAux).append("\n");
                //Codigo.acum.append(ifAux2);
                break;
            default:
                throw new IllegalArgumentException("Operador: " + operadorLogico + "Invalido");
        }
        ifAux1 = null;
        ifAux2 = null;
    }

}
