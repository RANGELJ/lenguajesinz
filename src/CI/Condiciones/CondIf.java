/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CI.Condiciones;

import CI.CIEtiqueta;
import CI.CIGoto;
import CI.CIIf;
import CI.Codigo;
import CI.Etiquetas;
import cuadruples.Cuadruple;
import cuadruples.Cuadruples;

/**
 *
 * @author anna
 */
public class CondIf {
    public static NodoC cond1=null;
    public static NodoC cond2=null;
    public static NodoC ref=null;
    
    
    
    public static void insertaOperadorLogico(String operador) {
        // System.out.println("Registra operador: " + operador);
        NodoC nuevoNodo=new NodoC();
        nuevoNodo.operalog=operador;
        
        if (!"NOT".equals(nuevoNodo.operalog)) {
            if(ref==null){
            nuevoNodo.h1=cond1;
            nuevoNodo.h2=cond2;
            cond1=null;
            cond2=null;
            ref=nuevoNodo;
            } else {
                nuevoNodo.h1=ref;
                nuevoNodo.h2=cond1;
                cond1=null;
                ref=nuevoNodo;
            }
        } else {
            // Condiciones para el not
            if (cond1 != null && cond2 == null) {
                nuevoNodo.h1 = cond1;
                cond1 = nuevoNodo;
            } else if (cond1 != null && cond2 != null) {
                nuevoNodo.h1 = cond2;
                cond2 = nuevoNodo;
            } else if (cond1 == null && cond2 == null) {
                nuevoNodo.h1 = ref;
                ref = nuevoNodo;
            }
        }
    }
    
    public static void registrarIf(String operando1, String operador, String operando2) {
        if(cond1==null){
            NodoC nuevoNodo=new NodoC();
            nuevoNodo.valor=operando1+operador+operando2;
            nuevoNodo.valoresIf = new String[]{operando1,operador,operando2};
            cond1=nuevoNodo;
        }
        else if(cond2==null){
            NodoC nuevoNodo=new NodoC();
            nuevoNodo.valor=operando1+operador+operando2;
            nuevoNodo.valoresIf = new String[]{operando1,operador,operando2};
            cond2=nuevoNodo;
            
        }
    }
    
    
    
    public static void iniciarGeneradorCondiciones() {
        cond1 = null;
        cond2 = null;
        ref = null;
    }
    
    public static void pegarIfs() throws Exception{
        if (ref == null && cond1 != null) {
            ref = cond1;
        }
        validarNodos(Etiquetas.getVerdadera(),Etiquetas.getFalsa(),ref);
    }
    private static void validarNodos(int etiquetaV, int etiquetaF, NodoC nodo ){
        if(nodo.operalog!=null){
            //es un OL 
            switch(nodo.operalog){
                case "OR":
                    int etiquetaF2=Etiquetas.nueva();
                    validarNodos(etiquetaV,etiquetaF2,nodo.h1);
                    Codigo.elements.add(new CIEtiqueta(etiquetaF2));
                    validarNodos(etiquetaV,etiquetaF,nodo.h2);
                    break;
                case "AND":
                    int etiquetaV1=Etiquetas.nueva();
                    validarNodos(etiquetaV1,etiquetaF,nodo.h1);
                    Codigo.elements.add(new CIEtiqueta(etiquetaV1));
                    validarNodos(etiquetaV,etiquetaF,nodo.h2);
                    break;
                case "NOT":
                    validarNodos(etiquetaF, etiquetaV, nodo.h1);
            }
        }else if(nodo.valor!=null){
            //es una condicion
            Codigo.elements.add(new CIIf(
                    nodo.valoresIf[0],
                    nodo.valoresIf[1],
                    nodo.valoresIf[2],
                    etiquetaV));
            Codigo.elements.add(new CIGoto(etiquetaF));
            Cuadruple cuadruple = new Cuadruple();
            cuadruple.setOperador("goto");
            cuadruple.setResultado(etiquetaV+"");
            Cuadruples.agregarCuadruple(cuadruple);
            cuadruple = new Cuadruple();
            cuadruple.setOperador("goto");
            cuadruple.setResultado(etiquetaF+"");
            Cuadruples.agregarCuadruple(cuadruple);
        }
    }
}
