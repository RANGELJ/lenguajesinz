/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CI.Condiciones;

/**
 *
 * @author anna
 */
public class IfOrOperadorLogico {
    
    private final SentIf sentenciaIf;
    private final String operadorLogico;
    
    public IfOrOperadorLogico(String operadorLogico) {
        this.operadorLogico = operadorLogico;
        this.sentenciaIf = null;
    }
    
    public IfOrOperadorLogico(SentIf sentenciaIf) {
        this.operadorLogico = null;
        this.sentenciaIf = sentenciaIf;
    }
    
    public boolean esOperadorLogico() {
        return this.operadorLogico != null && this.sentenciaIf == null;
    }
    
    public boolean esSentenciaIf() {
        return this.operadorLogico == null && this.sentenciaIf != null;
    }

    public SentIf getSentenciaIf() {
        return sentenciaIf;
    }

    public String getOperadorLogico() {
        return operadorLogico;
    }
    
    
    
}
