/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CI;

import java.util.LinkedList;

/**
 */
public class CIElement {
    
    private boolean activo;
    
    public static LinkedList<String> temporalesAgregados = new LinkedList();

    public CIElement() {
        this.activo = true;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
    
    public boolean esUnNombreTemporal(String nombre) {
        return nombre.matches("Temp[0-9]+");
    }

    public boolean esUnaCadenaLiteral(String nombre) {
        return nombre.matches("\"[^\"]*\"");
    }
    
    public void agregaDeclaracionTemporal(StringBuilder data, String temporal) {
        for (String tempAgregado : CIElement.temporalesAgregados) {
            if (tempAgregado.equals(temporal)) {
                return;
            }
        }
        
        data.append(temporal).append(" dw ?\n");
        CIElement.temporalesAgregados.add(temporal);
    }
    
    public void sustitulleReferencia(String nombreTemporal, String valor) {
        return;
    }
    
    public boolean referenciaEtiqueta(int etiqueta) {
        return false;
    }

    public void addToEnsamblador(StringBuilder codigo, StringBuilder data) {
        return;
    }

    public void addSaltoDeLineaEnConsola(StringBuilder contenedor) {
        contenedor.append(";Coloca un salto de linea despues de la lectura\n");
        contenedor.append("LEA DX, saltoLN\n");
        contenedor.append("MOV AH,09h\n");
        contenedor.append("INT 21h\n");
        contenedor.append(";---------------------------\n");
    }
    
    public String getRaw() {
        return "";
    }

}
