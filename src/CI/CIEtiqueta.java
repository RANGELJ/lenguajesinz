/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CI;

/**
 *
 * @author anna
 */
public class CIEtiqueta extends CIElement {
    
    private final int destino;

    public CIEtiqueta(int destino) {
        this.destino = destino;
    }

    @Override
    public void addToEnsamblador(StringBuilder codigo, StringBuilder data) {
        codigo.append("n"+this.destino+":\n");
        return;
    }
    
    @Override
    public String getRaw() {
        return this.destino + ":";
    }

    public int getDestino() {
        return destino;
    }
    
}
