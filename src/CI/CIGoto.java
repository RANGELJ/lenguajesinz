/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CI;

/**
 *
 * @author anna
 */
public class CIGoto extends CIElement {
    
    private final int destino;

    public CIGoto(int destino) {
        this.destino = destino;
    }

    @Override
    public void addToEnsamblador(StringBuilder codigo, StringBuilder data) {
        codigo.append("JMP n"+this.destino+"\n");
        return;
    }
    
    @Override
    public String getRaw() {
        return "goto " + this.destino;
    }
    
    @Override
    public boolean referenciaEtiqueta(int etiqueta) {
        return this.destino == etiqueta;
    }

    public int getDestino() {
        return destino;
    }

}
