/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CI;

/**
 *
 * @author anna
 */
public class CIIf extends CIElement {
    
    private final String operando1;
    private final String operador;
    private final String operando2;
    private final int destino;

    public CIIf(String operando1, String operador, String operando2, int destino) {
        this.operando1 = operando1;
        this.operador = operador;
        this.operando2 = operando2;
        this.destino = destino;
    }
    
    @Override
    public boolean referenciaEtiqueta(int etiqueta) {
        return this.destino == etiqueta;
    }

    @Override
    public void addToEnsamblador(StringBuilder codigo, StringBuilder data) {
        codigo.append(";Inicia sentencia if\n");
        codigo.append("MOV CX, "+this.operando1+"\n");
        codigo.append("CMP CX, "+this.operando2+"\n");

        switch(this.operador) {
            case ">=":
                codigo.append("JAE n"+this.destino+"\n");
                break;
            case "<=":
                codigo.append("JBE n"+this.destino+"\n");
                break;
            case "==":
                codigo.append("JE n"+this.destino+"\n");
                break;
            case ">":
                codigo.append("JA n"+this.destino+"\n");
                break;
            case "<":
                codigo.append("JB n"+this.destino+"\n");
                break;
            default:
                codigo.append(";Necesito diccionario de JUMPS para generar esta parte ;)\n");
                break;
        }
        return;
    }
    
    @Override
    public String getRaw() {
        return "if " + this.operando1 + " " + this.operador + " " + this.operando2
                + " goto "+destino;
    }

}
