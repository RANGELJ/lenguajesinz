/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CI;

/**
 *
 * @author anna
 */
public class CIDeclaracion extends CIElement {
    
    private final String tipo;
    private final String identificador;

    public CIDeclaracion(String tipo, String identificador) {
        this.tipo = tipo;
        this.identificador = identificador;
    }

    @Override
    public void addToEnsamblador(StringBuilder codigo, StringBuilder data) {
        switch(this.tipo) {
            case "INT":
                data.append(this.identificador).append(" dw ").append("?\n");
                break;
            case "STRING":
                data.append(this.identificador).append(" db ").append("\"\"\n");
                break;
            default:
                System.out.println("Tipo de declaracion no soportada "
                    + "para codigo ensamblador: " + this.tipo);
                return;
        }
        return;
    }
    
    @Override
    public String getRaw() {
        return this.tipo + " " + this.identificador;
    }
}
