/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CI;

/**
 *
 * @author anna
 */
public class CIAsignacionDoble extends CIAsignacionSimple {
    
    private final String operador;
    protected String valor2;

    public CIAsignacionDoble(String asignaA, String valor1, String operador, String valor2) {
        super(asignaA, valor1);
        this.operador = operador;
        this.valor2 = valor2;
    }

    public CIAsignacionDoble clone() {
        CIAsignacionDoble clone = new CIAsignacionDoble(this.getAsignaA(),
            this.getValor1(), this.operador, this.valor2);
        return clone;
    }

    public String getOperador() {
        return operador;
    }

    public String getValor2() {
        return valor2;
    }

    @Override
    public void addToEnsamblador(StringBuilder codigo, StringBuilder data) {
        codigo.append(";Asignacion doble\n");
        switch(this.operador) {
            case "+":
                codigo.append(";Suma dos numeros ("+this.getValor1()+" "+this.getValor2()+")\n");
                codigo.append("MOV AX,"+this.getValor1()+"\n");
                codigo.append("MOV CX,"+this.getValor2()+"\n");
                codigo.append("ADD AX,CX\n");
                codigo.append("MOV "+this.getAsignaA()+", AX\n");
                codigo.append(";-------------------------------\n");
                break;
            case "-":
                codigo.append(";Resta dos numeros ("+this.getValor1()+" "+this.getValor2()+")\n");
                codigo.append(";Guarda el resultado en : "+this.getAsignaA()+"\n");
                codigo.append("MOV AX,"+this.getValor1()+"\n");
                codigo.append("MOV CX,"+this.getValor2()+"\n");
                codigo.append("SUB AX,CX\n");
                codigo.append("MOV "+this.getAsignaA()+", AX\n");
                codigo.append(";-------------------------------\n");
                break;
            case "*":
                codigo.append("MOV AX,"+this.getValor1()+"\n");
                codigo.append("MOV BX,"+this.getValor2()+"\n");
                codigo.append("MUL BX\n");
                codigo.append("MOV "+this.getAsignaA()+",AX\n");
                break;
            case "/":
                codigo.append("MOV DX,0\n");
                codigo.append("MOV AX,"+this.getValor1()+"\n");
                codigo.append("MOV CX,"+this.getValor2()+"\n");
                codigo.append("DIV CX\n");
                codigo.append("MOV "+this.getAsignaA()+",AX\n");
                break;
            default:
                codigo.append(";Operador no soportado: ["+this.operador+"]\n");
                break;
        }

        if (this.esUnNombreTemporal(this.getAsignaA())) {
            this.agregaDeclaracionTemporal(data, this.getAsignaA());
        }
        if (this.esUnNombreTemporal(this.getValor1())) {
            this.agregaDeclaracionTemporal(data, this.getValor1());
        }
        if (this.esUnNombreTemporal(this.getValor2())) {
            this.agregaDeclaracionTemporal(data, this.getValor2());
        }

        return;
    }
    
    @Override
    public void sustitulleReferencia(String nombreTemporal, String valor) {
        super.sustitulleReferencia(nombreTemporal, valor);
        if (this.getValor2().equals(nombreTemporal)) {
            this.valor2 = valor;
        }
    }
    
    @Override
    public String getRaw() {
        return super.getRaw() + " " + this.operador + " " + this.valor2;
    }
}
