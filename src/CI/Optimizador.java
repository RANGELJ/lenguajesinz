/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CI;

import java.util.LinkedList;
import java.util.stream.Collectors;

/**
 *
 * @author anna
 */
public class Optimizador {
    
    private static boolean tieneCambios;
    
    public static void optimizar() {
        do {
            tieneCambios = false;
            Optimizador.quitarGotoRedundante(Codigo.elements);
            Optimizador.quitarEtiquetaSinReferencias(Codigo.elements);
            Codigo.elements = Optimizador.reduceMultiplicacionPor1(Codigo.elements);
            Codigo.elements = Optimizador.reduceMultiplicacionPor0(Codigo.elements);
            Codigo.elements = Optimizador.reducaMultiplicacionPor2(Codigo.elements);
            Codigo.elements = Optimizador.reduceDivicionEntre1(Codigo.elements);
            Codigo.elements = Optimizador.reduceSumaMasCero(Codigo.elements);
            Codigo.elements = Optimizador.reduceRestaA0(Codigo.elements);
            Codigo.elements = Optimizador.reduceOperable(Codigo.elements);
            Optimizador.quitaReferenciasATemporalesAsignacionUnica(Codigo.elements);
        } while(tieneCambios);  
    }

    private static void quitaReferenciasATemporalesAsignacionUnica(LinkedList<CIElement> elementos) {
        elementos.forEach(elemento -> {
            if (elemento.isActivo() && elemento instanceof CIAsignacionSimple
                && !(elemento instanceof CIAsignacionDoble) ) {
                CIAsignacionSimple asignacionSimple = ((CIAsignacionSimple) elemento);
                if (asignacionSimple.esUnNombreTemporal(asignacionSimple.getAsignaA())) {
                    System.out.println("Asignacion simple a temporal: " + elemento.getRaw());
                    String nombreTemporal = asignacionSimple.getAsignaA();
                    String nuevoValor = asignacionSimple.getValor1();
                    elementos.forEach(elemento2 -> {
                        if (elemento != elemento2 && elemento2.isActivo()) {
                            elemento2.sustitulleReferencia(nombreTemporal, nuevoValor);
                        }
                    });
                    elemento.setActivo(false);
                    tieneCambios = true;
                }
            }
        });
    }
    
    private static LinkedList<CIElement> reduceOperable(LinkedList<CIElement> elementos) {
        return elementos.stream()
            .filter(element -> element.isActivo())
            .map(element -> {
                if (element instanceof CIAsignacionDoble) {
                    CIAsignacionDoble asignacion = (CIAsignacionDoble) element;
                    String valor1 = asignacion.getValor1();
                    String valor2 = asignacion.getValor2();
                    
                    String reguexInteger = "(-)?[0-9]+";
                    String reguexDouble = "(-)?[0-9]*(.[0-9]+)?";
                    
                    boolean operacionNoCambiaTipo =
                         asignacion.getOperador().equals("+")
                         || asignacion.getOperador().equals("-")
                         || asignacion.getOperador().equals("*");
                    
                    if (valor1.matches(reguexInteger) && valor2.matches(reguexInteger)
                            && operacionNoCambiaTipo) {
                        // Se puede operar como enteros
                        int valor1Entero = Integer.parseInt(valor1);
                        int valor2Entero = Integer.parseInt(valor2);
                        
                        int valorNuevo;
                        switch(asignacion.getOperador()) {
                            case "+": valorNuevo = valor1Entero + valor2Entero; break;
                            case "-": valorNuevo = valor1Entero - valor2Entero; break;
                            case "*": valorNuevo = valor1Entero * valor2Entero; break;
                            default: throw new RuntimeException("Error");
                        }
                        
                        System.out.println("Operable: " + asignacion.getRaw());
                        tieneCambios = true;
                        return new CIAsignacionSimple(asignacion.getAsignaA(), valorNuevo + "");
                    }
                }
                return element;
            })
            .collect(Collectors.toCollection(LinkedList::new));
    }
    
    private static LinkedList<CIElement> reduceRestaA0(LinkedList<CIElement> elementos) {
        return elementos.stream()
            .filter(element -> element.isActivo())
            .map(element -> {
                if (element instanceof CIAsignacionDoble) {
                    CIAsignacionDoble asignacion = (CIAsignacionDoble) element;
                    if ("-".equals(asignacion.getOperador())) {
                        if (asignacion.getValor2().equals("0")) {
                            System.out.println("Resta 0: " + element.getRaw());
                            tieneCambios = true;
                            return new CIAsignacionSimple(asignacion.getAsignaA(), asignacion.getValor1());
                        }
                    }
                }
                return element;
            })
            .collect(Collectors.toCollection(LinkedList::new));
    }
    
    private static LinkedList<CIElement> reduceDivicionEntre1(LinkedList<CIElement> elementos) {
        return elementos.stream()
            .filter(element -> element.isActivo())
            .map(element -> {
                if (element instanceof CIAsignacionDoble) {
                    CIAsignacionDoble asignacion = (CIAsignacionDoble) element;
                    if ("/".equals(asignacion.getOperador())) {
                        if (asignacion.getValor2().equals("1")) {
                            System.out.println("Divicion entre 1: " + element.getRaw());
                            tieneCambios = true;
                            return new CIAsignacionSimple(asignacion.getAsignaA(), asignacion.getValor1());
                        }
                    }
                }
                return element;
            })
            .collect(Collectors.toCollection(LinkedList::new));
    }
    
    private static LinkedList<CIElement> reduceSumaMasCero(LinkedList<CIElement> elementos) {
        return elementos.stream()
            .filter(element -> element.isActivo())
            .map(element -> {
                if (element instanceof CIAsignacionDoble) {
                    CIAsignacionDoble asignacion = (CIAsignacionDoble) element;
                    if ("+".equals(asignacion.getOperador())) {
                        if (asignacion.getValor1().equals("0")) {
                            System.out.println("Suma mas 0: " + element.getRaw());
                            tieneCambios = true;
                            return new CIAsignacionSimple(asignacion.getAsignaA(), asignacion.getValor2());
                        } else if (asignacion.getValor2().equals("0")) {
                            System.out.println("Suma mas 0: " + element.getRaw());
                            tieneCambios = true;
                            return new CIAsignacionSimple(asignacion.getAsignaA(), asignacion.getValor1());
                        }
                    }
                }
                return element;
            })
            .collect(Collectors.toCollection(LinkedList::new));
    }
    
    private static LinkedList<CIElement> reducaMultiplicacionPor2(LinkedList<CIElement> elementos) {
        return elementos.stream()
            .filter(element -> element.isActivo())
            .map(element -> {
                if (element instanceof CIAsignacionDoble) {
                    CIAsignacionDoble asignacion = (CIAsignacionDoble) element;
                    if ("*".equals(asignacion.getOperador())) {
                        if (asignacion.getValor1().equals("2")) {
                            System.out.println("Multiplicacion por 2: " + element.getRaw());
                            tieneCambios = true;
                            return new CIAsignacionDoble(asignacion.getAsignaA(),
                                asignacion.getValor2(), "+", asignacion.getValor2());
                        } else if (asignacion.getValor2().equals("2")) {
                            System.out.println("Multiplicacion por 2: " + element.getRaw());
                            tieneCambios = true;
                            return new CIAsignacionDoble(asignacion.getAsignaA(),
                                asignacion.getValor1(), "+", asignacion.getValor1());
                        }
                    }
                }
                return element;
            })
            .collect(Collectors.toCollection(LinkedList::new));
    }
    
    private static LinkedList<CIElement> reduceMultiplicacionPor0(LinkedList<CIElement> elementos){
        return elementos.stream()
            .filter(element -> element.isActivo())
            .map(element -> {
                if (element instanceof CIAsignacionDoble) {
                    CIAsignacionDoble asignacion = (CIAsignacionDoble) element;
                    if ("*".equals(asignacion.getOperador())) {
                        if (asignacion.getValor1().equals("0")) {
                            System.out.println("Multiplicacion por 0: " + element.getRaw());
                            tieneCambios = true;
                            return new CIAsignacionSimple(asignacion.getAsignaA(), "0");
                        } else if (asignacion.getValor2().equals("0")) {
                            System.out.println("Multiplicacion por 0: " + element.getRaw());
                            tieneCambios = true;
                            return new CIAsignacionSimple(asignacion.getAsignaA(), "0");
                        }
                    }
                }
                return element;
            })
            .collect(Collectors.toCollection(LinkedList::new));
    }
    
    private static LinkedList<CIElement> reduceMultiplicacionPor1(LinkedList<CIElement> elementos) {
        return elementos.stream()
            .filter(element -> element.isActivo())
            .map(element -> {
                if (element instanceof CIAsignacionDoble) {
                    CIAsignacionDoble asignacion = (CIAsignacionDoble) element;
                    if ("*".equals(asignacion.getOperador())) {
                        if (asignacion.getValor1().equals("1")) {
                            System.out.println("Multiplicacion por 1: " + element.getRaw());
                            tieneCambios = true;
                            return new CIAsignacionSimple(asignacion.getAsignaA(), asignacion.getValor2());
                        } else if (asignacion.getValor2().equals("1")) {
                            System.out.println("Multiplicacion por 1: " + element.getRaw());
                            tieneCambios = true;
                            return new CIAsignacionSimple(asignacion.getAsignaA(), asignacion.getValor2());
                        }
                    }
                }
                return element;
            })
            .collect(Collectors.toCollection(LinkedList::new));
    }
    
    private static void quitarEtiquetaSinReferencias(LinkedList<CIElement> elementos) {
        elementos.forEach(elemento -> {
            if (elemento.isActivo() && elemento instanceof CIEtiqueta) {
                int etiqueta = ((CIEtiqueta) elemento).getDestino();
                long numeroElementosReferencian = elementos.stream()
                    .filter(elemento2 ->
                        elemento2.referenciaEtiqueta(etiqueta) && elemento2.isActivo()).count();
                if (numeroElementosReferencian < 1) {
                    System.out.println("Etiqueta sin referencias: " + etiqueta);
                    tieneCambios = true;
                    elemento.setActivo(false);
                }
            }
        });
    }
    
    private static void quitarGotoRedundante(LinkedList<CIElement> elementos) {
        for (int index = 0; index < elementos.size(); index++) {
            CIElement elementoActual = elementos.get(index);
            if (! elementoActual.isActivo()) {
                continue;
            }
            
            int indexSiguiente = index + 1;
            
            if (indexSiguiente >= elementos.size()) {
                // Ya no hay mas elementos siguientes
                break;
            }
            
            CIElement elementoSiguiente = elementos.get(indexSiguiente);
            
            if (elementoActual instanceof CIGoto && elementoSiguiente instanceof CIEtiqueta) {
                int destinoGoto = ((CIGoto) elementoActual).getDestino();
                int destinoEtiqueta = ((CIEtiqueta) elementoSiguiente).getDestino();
                boolean tienenMismaEtiqueta = destinoGoto == destinoEtiqueta;
                
                if (tienenMismaEtiqueta) {
                    // Es un goto redundante y no debe colocarce
                    System.out.println("Encuentra goto redundante: " + elementoActual.getRaw());
                    tieneCambios = true;
                    elementoActual.setActivo(false);
                }
            }
        }
    }
    
}
