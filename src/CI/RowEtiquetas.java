/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CI;

/**
 *
 * @author mayro
 */
public class RowEtiquetas {
    private final Integer ev;
    private final Integer ef;
    private final Integer ei;
    private final Integer eaux;
    private final Integer nivel;
    private final Integer tipo;
    
    public static final int IF = 0;
    public static final int WHILE = 1;
    public static final int FOR = 2;

    public RowEtiquetas(int ev, int ef, int ei, int eaux, int nivel,int tipo) {
        this.ev = ev;
        this.ef = ef;
        this.ei = ei;
        this.eaux = eaux;
        this.nivel = nivel;
        this.tipo=tipo;
    }

    public Integer getTipo() {
        return tipo;
    }

    public int getVerdadera() {
        return ev;
    }

    public int getFalsa() {
        return ef;
    }

    public int getInicial() {
        return ei;
    }

    public int getNivel() {
        return nivel;
    }

    public Integer getAuxiliar() {
        return eaux;
    }
    
    
}
