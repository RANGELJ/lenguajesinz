/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CI;

import java.util.LinkedList;
import lector.Token;
import lector.TokenV;

/**
 *
 * @author mayro
 */
public class Codigo {

    public static LinkedList<CIElement> elements = new LinkedList();
    
    public static void limpiar(){
        Etiquetas.limpiar();
        elements.clear();
        
    }
    
    public static String getStringCodigo(){
        StringBuilder acum = new StringBuilder();
        elements.forEach(cielement -> {
            if (cielement.isActivo()) {
                acum.append(cielement.getRaw()).append("\n");
            }
        });
        return acum.toString();
    }

    public static String getCodigoEnsamblador() {
        StringBuilder header = new StringBuilder();
        StringBuilder data = new StringBuilder();
        StringBuilder codigo = new StringBuilder();

        header.append("\n\norg 100h\n\n");

        header.append("include \"emu8086.inc\"\n");
        header.append("DEFINE_PRINT_STRING\n");
        header.append("DEFINE_PRINT_NUM\n");
        header.append("DEFINE_PRINT_NUM_UNS\n");
        header.append("DEFINE_SCAN_NUM\n\n");

        header.append("TITLE Programa generado desde lenguaje sin Z\n");
        header.append(".MODEL large\n");
        header.append(".STACK 64\n");

        data.append(".DATA\n");
        data.append(";Variable para hacer saltos de linea\n");
        data.append("saltoLN db 10,13,\"$\"\n");

        codigo.append(".CODE\n");
        codigo.append("inicioprograma PROC FAR\n");
        codigo.append(";se direcciona al segmento donde se guardan las variables\n");
        codigo.append("MOV AX,@data\n");
        codigo.append("MOV DS,AX\n");

        elements.forEach(cielement -> {
            if (cielement.isActivo()) {
                cielement.addToEnsamblador(codigo, data);
            }
        });
        codigo.append("MOV AH,4ch\n");
        codigo.append("INT 21h\n");
        codigo.append("inicioprograma ENDP\n");
        codigo.append("END\n");

        return header.append(data).append(codigo).toString();
    }
    
    public static String extract(Token ref){
        String refVal=ref.getValor();
        if(ref instanceof TokenV){
            refVal=((TokenV) ref).getIdentificador();
        }
        return refVal;
    }
}
