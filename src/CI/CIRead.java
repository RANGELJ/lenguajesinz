/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CI;

/**
 *
 * @author anna
 */
public class CIRead extends CIElement {
    
    private final String apuntador;

    public CIRead(String apuntador) {
        this.apuntador = apuntador;
    }

    @Override
    public void addToEnsamblador(StringBuilder codigo, StringBuilder data) {
        codigo.append(";Lee un dato\n");
        codigo.append("call SCAN_NUM\n");
        codigo.append("MOV "+this.apuntador+", CX\n");
        this.addSaltoDeLineaEnConsola(codigo);
        return;
    }
    
    @Override
    public String getRaw() {
        return "read " + this.apuntador;
    }
    
}
