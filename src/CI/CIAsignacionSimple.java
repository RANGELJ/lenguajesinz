/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CI;

/**
 *
 * @author anna
 */
public class CIAsignacionSimple extends CIElement {

    private String asignaA;
    private String valor1;

    public CIAsignacionSimple(String asignaA, String valor1) {
        this.asignaA = asignaA;
        this.valor1 = valor1;
    }

    public void setAsignaA(String asignaA) {
        this.asignaA = asignaA;
    }

    public String getAsignaA() {
        return asignaA;
    }

    public String getValor1() {
        return valor1;
    }

    public CIAsignacionSimple clone() {
        CIAsignacionSimple clone = new CIAsignacionSimple(this.asignaA, this.valor1);
        return clone;
    }

    @Override
    public void addToEnsamblador(StringBuilder codigo, StringBuilder data) {
        codigo.append(";Asignacion simple\n");
        if (this.esUnaCadenaLiteral(this.valor1)) {
            int nuevaEtiqueta = Etiquetas.nueva();
            String identificadorMensaje = "ms"+nuevaEtiqueta;

            // Declaracion en data
            data.append(identificadorMensaje).append(" db 10,13,")
                .append(this.valor1).append(",'$'\n");

            // Movimiento a variable
            codigo.append("MOV "+this.asignaA+","+identificadorMensaje+"\n");
        } else {
            codigo.append("MOV DX,"+this.getValor1()+"\n");
            codigo.append("MOV "+this.asignaA+",DX\n");
        }
        
        if (this.esUnNombreTemporal(this.getAsignaA())) {
            this.agregaDeclaracionTemporal(data, this.getAsignaA());
        }
        if (this.esUnNombreTemporal(this.getValor1())) {
            this.agregaDeclaracionTemporal(data, this.getValor1());
        }
        return;
    }
    
    @Override
    public void sustitulleReferencia(String nombreTemporal, String valor) {
        if (this.getValor1().equals(nombreTemporal)) {
            this.valor1 = valor;
        }
    }
    
    @Override
    public String getRaw() {
        return this.asignaA + " = " + this.valor1;
    }

}
