/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cuadruples;

/**
 *
 * @author 
 */
public class Cuadruple {

    private String operando1;
    private String operando2;
    private String operador;
    private String resultado;

    public Cuadruple() {
    }
    
    @Override
    public String toString() {
        return "Cuadruple: " + "Opn1["+operando1+"]"+" Opn2["+operando2+"]"
                +" Op["+operador+"] "+"Res["+resultado+"]";
    }

    public String getOperando1() {
        return operando1;
    }

    public void setOperando1(String operando1) {
        this.operando1 = operando1;
    }

    public String getOperando2() {
        return operando2;
    }

    public void setOperando2(String operando2) {
        this.operando2 = operando2;
    }

    public String getOperador() {
        return operador;
    }

    public void setOperador(String operador) {
        this.operador = operador;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }
    
    
}
