/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cuadruples;

import java.util.LinkedList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author
 */
public class Cuadruples {
    
    private static LinkedList<Cuadruple> lista = new LinkedList();
    
    public static void limpiar() {
        lista.clear();
    }
    
    public static void agregarCuadruple(Cuadruple nuevoCuadruple) {
        lista.add(nuevoCuadruple);
    }

    public static void imprimirLista(JTable tablaUI) {
        
        TableColumnModel columnModel =tablaUI.getTableHeader()
                .getColumnModel();
       
        String[] nombreColumnas = {"Operador 1", "Operador 2", "Operador", "Resultado"};
        
        DefaultTableModel tableModel = new DefaultTableModel(0, 0);
        
        tableModel.setColumnIdentifiers(nombreColumnas);

        lista.forEach((cuadruple) -> {
            tableModel.addRow(new Object[]{
                cuadruple.getOperando1(),
                cuadruple.getOperando2(),
                cuadruple.getOperador(),
                cuadruple.getResultado()
            });
        });
        
        tablaUI.setModel(tableModel);
    }

}
